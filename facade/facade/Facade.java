package facade;

import subsystems.Check_liquids;
import subsystems.Check_mirrors;
import subsystems.Check_seats;
import subsystems.Start_car;

public class Facade implements IFacade{

	private Check_liquids	check_liquids;
	private Check_mirrors	check_mirrors;
	private Check_seats		check_seats;
	private Start_car		start_car;
	
	public Facade() {
		check_liquids = new Check_liquids();
		check_mirrors = new Check_mirrors();
		check_seats = new Check_seats();
		start_car = new Start_car();
	}
	
	@Override
	public void Start_the_car() {
		check_liquids.Start();
		check_mirrors.Start();
		check_seats.Start();
		start_car.Start();
	}

	

}
