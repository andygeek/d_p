package client;

import facade.Facade;

public class Main {

	public static void main(String[] args) {
		
		Facade facade = new Facade();
		
		facade.Start_the_car();
		System.out.println("\nPROCESS FINISHED");
	}

}
