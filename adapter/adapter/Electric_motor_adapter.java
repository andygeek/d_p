package adapter;

import motors.Electric_motor;
import motors.Motor;

public class Electric_motor_adapter extends Motor {

	private Electric_motor electric_motor;
	
	public Electric_motor_adapter() {
		super();
		this.electric_motor = new Electric_motor();
	}
	
	@Override
	public void turn_on() {
		System.out.println("Turn on electric motor");
		electric_motor.connect();
		electric_motor.activate();
	}

	@Override
	public void speed_up() {
		electric_motor.speed_up_motor();
	}

	@Override
	public void turn_off() {
		electric_motor.disconnect();
	}

}
