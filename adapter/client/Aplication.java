package client;

import adapter.Electric_motor_adapter;
import motors.Common_motor;
import motors.Economic_motor;
import motors.Motor;

public class Aplication {
	public void use_common_motor() {
		Motor motor = new Common_motor();
		motor.turn_on();
		motor.speed_up();
		motor.turn_off();
	}
	public void use_electric_motor() {
		Motor motor = new Electric_motor_adapter();
		motor.turn_on();
		motor.speed_up();
		motor.turn_off();
	}
	public void use_economic_motor() {
		Motor motor = new Economic_motor();
		motor.turn_on();
		motor.speed_up();
		motor.turn_off();
	}
}
