package motors;

public abstract class Motor {
	abstract public void turn_on();
	abstract public void speed_up();
	abstract public void turn_off();
}
