package motors;

public class Electric_motor {
	private boolean connect = false;
	
	public Electric_motor() {
		System.out.println("Creating electric motor");
	}
	public void connect() {
		System.out.println("Connecting electric motor");
		this.connect = true;
	}
	public void activate() {
		if(this.connect == false) {
			System.out.println("!!The motor is disconnecd... ");
		}
		else {
			System.out.println("Activating electric motor");
		}
	}
	public void speed_up_motor() {
		if(this.connect == false) {
			System.out.println("!!The motor is disconnected... ");
		}
		else {
			System.out.println("Speed up electric motor");
		}
	}
	public void disconnect() {
		System.out.println("The motor is disconnected");
		connect = false;
	}
}
