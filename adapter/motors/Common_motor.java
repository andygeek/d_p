package motors;

public class Common_motor extends Motor {

	public Common_motor() {
		super();
		System.out.println("Creating common motor");
	}
	
	@Override
	public void turn_on() {
		System.out.println("Turn on common motor");
	}

	@Override
	public void speed_up() {
		System.out.println("Speed up common motor");
	}

	@Override
	public void turn_off() {
		System.out.println("Turn off common motor");
	}
	
}
