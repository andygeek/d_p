package motors;

public class Economic_motor extends Motor {

	public Economic_motor() {
		super();
		System.out.println("Creating economic motor");
	}
	
	@Override
	public void turn_on() {
		System.out.println("Turn on economic motor");
	}

	@Override
	public void speed_up() {
		System.out.println("Speed up economic motor");
	}

	@Override
	public void turn_off() {
		System.out.println("Turn off economic motor");
	}

}
